<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_type extends Model
{
    //
    public $table = 'users_type';

     protected $fillable = [
        'name',
    ];
}
