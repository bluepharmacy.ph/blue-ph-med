<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    public $table = 'products';

     protected $fillable = [
        'name',
        'unit_price',
        'sell_price',
        'unit_id',
        'sold_from',
        'classification_id',
        'locate_id',
        'stocks',
        'stocks_alert',
    ];
}
